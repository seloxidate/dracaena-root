use dracaena_root::root::AstRootNode;
use indoc::indoc;
use pretty_assertions::assert_eq;

#[test]
fn parse_transpile_print() -> Result<(), String> {
    let select_stmt = r#"select firstonly custTable
                        where custTable.AccountNum == '1234';"#;

    let ast_root: AstRootNode = select_stmt.parse().map_err(|err| format!("Parsing failed: {}", err))?;
    let query_as_code = ast_root.transpile_to_query().map(|ast| ast.code_fmt())?;
    let expected = indoc! {r#"
        QueryBuildDataSource qbdsCustTable;
        Query                query;
        QueryRun             queryRun;
        CustTable            custTable;
        query = new Query();
        qbdsCustTable = query.addDataSource(custTable.TableId);
        qbdsCustTable.firstOnly(true);
        qbdsCustTable.addRange(fieldNum(CustTable, AccountNum)).value('1234');
        queryRun = new QueryRun(query);
        queryRun.next();
        custTable = queryRun.get(custTable.TableId);
        "#};
    assert_eq!(expected, query_as_code);
    Ok(())
}