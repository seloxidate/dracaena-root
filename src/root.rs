use dracaena::ast::local_body::LocalBody;
use std::str::FromStr;
use dracaena::ast::{
    top_level_ast_node::TopLevelAstNode,
};

pub struct AstRootNode {
    ast_node: TopLevelAstNode,
}

impl AstRootNode {
    pub fn code_fmt(&self) -> String {
        use sprout::{
            pretty_printer::PrettyPrinter,
            pretty_printer_visitor::PrettyPrintOptions,
        };
        let mut pretty_printer = PrettyPrinter::new(Default::default());
        pretty_printer.pretty_print(&self.ast_node)
    }

    pub fn transpile_to_query(&self) -> Result<Self, String> {
        use selecuery::visitor::transpiler_visitor::TranspilerVisitor;
        use dracaena::visitor::visitable::Visitable;
        let mut transpiler_visitor = TranspilerVisitor::new();
        match &self.ast_node {
            TopLevelAstNode::SelectStmt(find_join) => {
                find_join.take_visitor_enter_leave(&mut transpiler_visitor);
                transpiler_visitor.into_transpiled_ast()
                    .map(|local_body| Self {
                        ast_node: TopLevelAstNode::LocalBody(local_body)
                    })
                    .ok_or("Transpilation to query expression failed.".into())
            },
            TopLevelAstNode::LocalBody(_local_body) => {
                // there is nothing to transpile here
                Err("The current code structure can't be transpiled into a query.".into())
            }
        }
    }
}

use selector::parsers::parser::*;
impl FromStr for AstRootNode {
    type Err = ParseError;
    fn from_str(str_to_parse: &str) -> std::result::Result<Self, Self::Err> {
        str_to_parse.parse::<SelectStmtParsed>()
            .map(|select_stmt_parsed| {
                AstRootNode {
                    ast_node: TopLevelAstNode::SelectStmt(select_stmt_parsed.ast)
                }
            })
    }
}